/*
 *  This is a CMAKE Generated version header file. It should not be changed manually
 * */
#ifndef INCLUDE_VERSION
#define INCLUDE_VERSION

#define PROJECT_NAME "cfm"
#define PROJECT_VER  "4.0.1"
#define PROJECT_VER_MAJOR "4"
#define PROJECT_VER_MINOR "0"
#define PTOJECT_VER_PATCH "1"

#endif // INCLUDE_VERSION
